<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Carrito_controller extends CI_Controller
{
    public function _construct()
    {
        parent::__construct();
    }

    public function ver_carrito()
    {
        if (!$this->cart->contents()) {
            $data['message'] = 'Carrito Vacio';
        } else {
            $data['message'] = '';
        }
        $data['titulo'] = 'Carrito de compras';

        $this->load->view('plantillas/head', $data);
        $this->load->view('plantillas/navbar');
        $this->load->view('contenidos/carrito_view', $data);
        $this->load->view('plantillas/footer');
    }

    public function agregar_carrito()
    {
        $data = array(
            'id' => $this->input->post('id'),
            'name' => $this->input->post('nombre'),
            'descripcion' => $this->input->post('descripcion'),
            'price' => $this->input->post('precio'),
            'qty' => 1
        );
        $this->cart->insert($data);
        redirect('carrito');
    }
    public function eliminar($id)
    {
        if ($id == "all") {
            $this->cart->destroy();
        } else {
            $data = array(
                'rowid' => $id,
                'qty' => 0
            );
            $this->cart->update($data);
        }
        redirect('carrito');
    }
}
