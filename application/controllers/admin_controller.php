<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller {
 function _construct() {
	parent::__construct();
	if(!$this->session->userdata('login')){
		redirect('usuarios_controller/iniciar_sesion');
	}
}

	public function indexAdmin(){
		$data['titulo'] = 'Game House - Admin';

		$this->load->view('plantillas/head', $data);
		$this->load->view('plantillas/navbar_admin');
		$this->load->view('contenidos/contenido_principal');
		$this->load->view('plantillas/footer');
	}
}