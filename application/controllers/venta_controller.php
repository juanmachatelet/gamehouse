<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Venta_controller extends CI_Controller
{
    function _construct()
    {

        parent::__construct();
    }

    public function guardar_venta()
    {
        $this->load->model('venta_model');
        $this->load->model('juego_model');
        $encabezado_venta = array(
            'cliente_id' => $this->session->userdata('id_usuario'),
            'venta_fecha' => date('Y-m-d'),
        );

        $this->venta_model->guardar_ventas($encabezado_venta);

        // Obtiene el ultimo id_venta

        $venta_id = $this->db->insert_id();

        $cart = $this->cart->contents();

        foreach ($cart as $item) {
            $detalle_venta = array(
                'id_venta' => $venta_id,
                'id_juego' => $item['id'],
                'detalle_cantidad' => $item['qty'],
                'detalle_precio' => $item['price']
            );

            // Controlar el stock

            $producto = $this->juego_model->get_juego_id($item['id']);

            $stock = $producto->juego_stock;

            if ($stock >= $item['qty']) {
                $stock = $stock - $item['qty'];
                $data = array(
                    'juego_stock' => $stock
                );
                $this->juego_model->actualizar_juego($data, $item['id']);
                $this->venta_model->guardar_detalle_ventas($detalle_venta);
            }
        }
        $this->cart->destroy();

        echo "<script>alert('Compra registrada correctamente. Muchas Gracias!');</script>";
        redirect('Proyecto_controller', 'refresh');
    }

    public function listar_ventas()
    {
        $this->load->model('venta_model');
        $this->load->model('usuario_model');
        $data['venta'] = $this->venta_model->select_ventas();
        $data['titulo'] = 'Listar ventas';
        $this->load->view('plantillas/head', $data);
        $this->load->view('plantillas/navbar_admin');
        $this->load->view('contenidos/listar_ventas', $data);
        $this->load->view('plantillas/footer');
    }
    
    public function detalle_ventas($id=NULL)
    {
        $this->load->model('venta_model');
        $data['detalle_venta'] = $this->venta_model->select_detalle_ventas($id);
        $data['titulo'] = 'Detalle de ventas';
        $this->load->view('plantillas/head', $data);
        $this->load->view('plantillas/navbar_admin');
        $this->load->view('contenidos/ver_detalles_venta', $data);
        $this->load->view('plantillas/footer');
    }

    public function filtrar_ventas(){
        $comienzo1 = $_POST['txtStartDate'];
        $comienzo2 = date("Y-m-d", strtotime($comienzo1));
        $final1 = $_POST['txtEndDate'];
        $final2 = date("Y-m-d", strtotime($final1));
        $this->load->model('venta_model');
        $data['venta'] = $this->venta_model->select_filtered_dates($comienzo2, $final2);
        $data['titulo'] = 'Listar ventas por fecha';
        $this->load->view('plantillas/head', $data);
        $this->load->view('plantillas/navbar_admin');
        $this->load->view('contenidos/listar_ventas', $data);
        $this->load->view('plantillas/footer');
    }
}
