<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Juego_controller extends CI_Controller
{
    public function _construct()
    {
        parent::__construct();
        if (!$this->session->userdata('login')) {
            redirect('usuarios_controller/iniciar_sesion');
        }
    }

    public function form_agregar_juego()
    {
        $this->load->model('juego_model');
        $data['consolas'] = $this->juego_model->select_consola();
        $data['titulo'] = 'Agregar juego';

        $this->load->view('plantillas/head', $data);
        $this->load->view('plantillas/navbar_admin');
        $this->load->view('contenidos/agregar_juego', $data);
        $this->load->view('plantillas/footer');
    }

    public function registrar_juego()
    {
        $this->form_validation->set_rules('titulo', 'Titulo del juego', 'required');
        $this->form_validation->set_rules('desarrollador', 'Desarrollador del juego', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripcion del juego', 'required');
        $this->form_validation->set_rules('stock', 'Stock', 'required|integer');
        $this->form_validation->set_rules('precio', 'Precio', 'numeric');
        $this->form_validation->set_rules('imagen', 'Seleccionar una imagen', 'callback_select_validate');
        $this->form_validation->set_rules('consola', 'Consola', 'required|callback_select_validate');
        $this->form_validation->set_message('numeric', 'Debes ingresar valores numericos');
        $this->form_validation->set_message('integer', 'El campo %s debe poseer solo numeros enteros');
        $this->form_validation->set_message('required', 'El campo %s es obligaorio');

        if ($this->form_validation->run() == FALSE) {
            $this->form_agregar_juego();
        } else {
            $this->guardar_juego();
        }
    }

    function validar_imagen($imagen)
    {
        //verifica que se ingreso una imagen
        $titulo_imagen = $_FILES['imagen']['name'];
        if (empty($titulo_imagen)) {
            $this->form_validation->set_message('validar_imagen', 'Selecciona una imagen');
            return false;
        } else {
            return true;
        }
    }

    function select_validate($consola)
    {
        if ($consola == "0") {
            $this->form_validation->set_message('select_validate', 'Seleccione una consola');
            return false;
        } else {
            return true;
        }
    }

    function guardar_juego()
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png|PNG';
        $config['remove_spaces'] = 'TRUE';
        $config['max_size'] = '2048000';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('imagen')) {
            echo "<script type= \"text/javascript\">alert('No se pudo cargar el archivo');</script>";
            $this->form_agregar_juego();
        } else {
            $data = array(
                'juego_titulo' => $this->input->post('titulo'),
                'juego_desarrollador' => $this->input->post('desarrollador'),
                'juego_descripcion' => $this->input->post('descripcion'),
                'juego_stock' => $this->input->post('stock'),
                'juego_precio' => $this->input->post('precio'),
                'juego_imagen' => $_FILES['imagen']['name'],
                'consola_id' => $this->input->post('consola'),
                'juego_estado' => 1
            );

            $this->load->model('juego_model');
            $this->juego_model->guardar_juego($data);
            redirect('agregar');
        }
    }

    public function gestionar_juego()
    {
        $this->load->model('juego_model');
        $data['juegos'] = $this->juego_model->select_juego();
        $data['consolas'] = $this->juego_model->select_consola();
        $data['titulo'] = 'Gestionar juego';
        $this->load->view('plantillas/head', $data);
        $this->load->view('plantillas/navbar_admin');
        $this->load->view('contenidos/gestionar_juego', $data);
        $this->load->view('plantillas/footer');
    }
    public function editar_juego($id = NULL)
    {
        $this->load->model('juego_model');
        $data['titulo'] = 'Editar juego';
        $data['consolas'] = $this->juego_model->select_consola();
        $juegos = $this->juego_model->select_juegos_id($id);
        foreach ($juegos as $row) {
            $data['juego_id'] = $row->juego_id;
            $data['titulo'] = $row->juego_titulo;
            $data['desarrollador'] = $row->juego_desarrollador;
            $data['descripcion'] = $row->juego_descripcion;
            $data['stock'] = $row->juego_stock;
            $data['precio'] = $row->juego_precio;
            $data['imagen'] = $row->juego_imagen;
            $data['estado'] = $row->juego_estado;
            $data['consola'] = $row->consola_id;
        }
        $this->load->view('plantillas/head', $data);
        $this->load->view('plantillas/navbar_admin');
        $this->load->view('contenidos/gestionar_juego_edicion', $data);
        $this->load->view('plantillas/footer');
    }

    public function actualizar_juego($id = NULL)
    {
        $this->form_validation->set_rules('titulo', 'Titulo del producto', 'required');
        $this->form_validation->set_rules('desarrollador', 'Desarrollador', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripcion del producto', 'required');
        $this->form_validation->set_rules('stock', 'Stock', 'required|integer');
        $this->form_validation->set_rules('precio', 'Precio', 'required|numeric');
        //$this->form_validation->set_rules('consola', 'consola', 'required|callback_select_validate');
        $this->form_validation->set_rules('estado', 'Estado', 'numeric');
        $this->form_validation->set_message('integer', 'El campo %s debe contener solo numeros enteros');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_message('numeric', 'Debe ingresar valores numericos');
        $this->load->model('juego_model');
        
        if ($this->form_validation->run() == FALSE) {
            $this->editar_juego($id);
        } else {
            $imagen = $_FILES['imagen']['name'];
            if(!empty($imagen)){
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png|PNG';
                $config['overwrite'] = 'TRUE';
                $config['remove_spaces'] = 'TRUE';
                $config['max_size'] = '2048000';

                $this->load->helper(array('form', 'url'));
                $this->load->library('upload', $config);
                $this->upload->do_upload('imagen'); 


                $data = array(
                    'juego_titulo' => $this->input->post('titulo'),
                    'juego_desarrollador' => $this->input->post('desarrollador'),
                    'juego_descripcion' => $this->input->post('descripcion'),
                    'juego_stock' => $this->input->post('stock'),
                    'juego_precio' => $this->input->post('precio'),
                    'consola_id' => $this->input->post('consola'),
                    'juego_imagen' => $imagen,
                );
            } else {
                
                $data = array(
                    'juego_titulo' => $this->input->post('titulo'),
                    'juego_desarrollador' => $this->input->post('desarrollador'),
                    'juego_descripcion' => $this->input->post('descripcion'),
                    'juego_stock' => $this->input->post('stock'),
                    'juego_precio' => $this->input->post('precio'),
                    'consola_id' => $this->input->post('consola')
                );
            }
            $this->load->model('juego_model');
            $this->juego_model->actualizar_juego($data, $id);
            redirect('Juego_controller/gestionar_juego');
        }
            // if (empty($imagen)) {

            //     $config['upload_path'] = './uploads/';
            //     $config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png|PNG';
            //     $config['overwrite'] = 'TRUE';
            //     $config['remove_spaces'] = 'TRUE';
            //     $config['max_size'] = '2048000';

            //     $this->load->helper(array('form', 'url'));
            //     $this->load->library('upload', $config);
            //     $this->upload->do_upload('imagen');


            //     $data = array(
            //         'juego_titulo' => $this->input->post('titulo'),
            //         'juego_desarrollador' => $this->input->post('desarrollador'),
            //         'juego_descripcion' => $this->input->post('descripcion'),
            //         'juego_stock' => $this->input->post('stock'),
            //         'juego_precio' => $this->input->post('precio'),
            //         'consola_id' => $this->input->post('consola'),
            //         'juego_imagen' => $this->input->post('imagen'),
            //     );
            // } else {
            //     $data = array(
            //         'juego_titulo' => $this->input->post('titulo'),
            //         'juego_desarrollador' => $this->input->post('desarrollador'),
            //         'juego_descripcion' => $this->input->post('descripcion'),
            //         'juego_stock' => $this->input->post('stock'),
            //         'juego_precio' => $this->input->post('precio'),
            //         'consola_id' => $this->input->post('consola'),
            //         'juego_imagen' => $imagen,
            //     );
            // }

            
        
    }

    public function actualizar_producto($id = NULL)
    {
        $this->form_validation->set_rules('titulo', 'Titulo del producto', 'required');
        $this->form_validation->set_rules('desarrollador', 'Desarrollador', 'required');
        $this->form_validation->set_rules('descripcion', 'Descripcion del producto', 'required');
        $this->form_validation->set_rules('stock', 'Stock', 'required|integer');
        $this->form_validation->set_rules('precio', 'Precio', 'required|numeric');
        $this->form_validation->set_rules('consola', 'consola', 'required|callback_select_validate');
        $this->form_validation->set_rules('estado', 'Estado', 'numeric');
        $this->form_validation->set_message('integer', 'El campo %s debe contener solo numeros enteros');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio');
        $this->form_validation->set_message('numeric', 'Debe ingresar valores numericos');

        if ($this->form_validation->run() == FALSE) {
            $this->editar_juego($id);
        } else {
            $imagen = $_FILES['imagen']['name'];

            if (!empty($imagen)) {

                $config['upload_path'] = '/uploads/';
                $config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png|PNG';
                $config['remove_spaces'] = 'TRUE';
                $config['max_size'] = '1024';

                $this->load->library('upload', $config);
                $this->upload->do_upload('imagen');

                $data = array(
                    'helado_nombre' => $this->input->post('nombre'),
                    'helado_desc' => $this->input->post('descripcion'),
                    'helado_stock' => $this->input->post('stock'),
                    'helado_precio' => $this->input->post('precio'),
                    'helado_categorias' => $this->input->post('categoria'),
                    'helado_imagen' => $imagen
                );
            } else {
                $data = array(
                    'helado_nombre' => $this->input->post('nombre'),
                    'helado_desc' => $this->input->post('descripcion'),
                    'helado_stock' => $this->input->post('stock'),
                    'helado_precio' => $this->input->post('precio'),
                    'helado_categorias' => $this->input->post('categoria')
                );
            }
            $this->load->model('Producto_model');
            $this->Producto_model->actualizar_helados($data, $id);
            redirect('Producto_controller/gestionar_producto');
        }
    }

    public function eliminar_juego($id = NULL)
    {

        $data = array(
            'juego_estado' => '0'
        );
        $this->load->model('juego_model');
        $this->juego_model->actualizar_juego($data, $id);
        redirect('Juego_controller/gestionar_juego');
    }

    public function activar_juego($id = NULL)
    {
        $data = array(
            'juego_estado' => '1'
        );
        $this->load->model('juego_model');
        $this->juego_model->actualizar_juego($data, $id);
        redirect('Juego_controller/gestionar_juego');
    }
}
