<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyecto_controller extends CI_Controller {
	public function _construct(){
		parent::__construct();
	}

	public function index()
	{
		$data['titulo'] = 'Game House';

		$this->load->view('plantillas/head', $data);
		$this->load->view('plantillas/navbar');
		$this->load->view('contenidos/contenido_principal');
		$this->load->view('plantillas/footer');
	}

	public function nosotros()
	{
		$data['titulo'] = 'Nosotros';
		
		$this->load->view('plantillas/head', $data);
		$this->load->view('plantillas/navbar');
		$this->load->view('contenidos/nosotros');
		$this->load->view('plantillas/footer');
	}

	public function catalogo()
	{
		$data['titulo'] = 'Catalogo';

		$this->load->model('juego_model');

		$data['juego'] = $this->juego_model->get_juegos();
	
		$this->load->view('plantillas/head', $data);
		$this->load->view('plantillas/navbar');
		$this->load->view('juegos/listar_juegos');
		$this->load->view('plantillas/footer');
	}

	public function condiciones()
	{
		$data['titulo'] = 'Términos y condiciones';

		$this->load->view('plantillas/head', $data);
		$this->load->view('plantillas/navbar');
		$this->load->view('contenidos/condiciones');
		$this->load->view('plantillas/footer');
	}
	
	
}

;?>