<h1 class="text-center mt-2 text-white">Catálogo de juegos</h1>
<div class="container">
    <table id="mytable" class="table table-bordered table-dark col-12">
        <thead>
            <th>Título</th>
            <th>Desarrollador</th>
            <th>Descripcion</th>
            <th>Stock</th>
            <th>Precio</th>
            <th>Consola</th>
        </thead>
        <tbody>
            <?php foreach ($juego as $row) { ?>
                <tr>
                    <td> <?php echo $row->juego_titulo; ?> </td>
                    <td> <?php echo $row->juego_desarrollador; ?> </td>
                    <td> <?php echo $row->juego_descripcion; ?> </td>
                    <td> <?php echo $row->juego_stock; ?> </td>
                    <td> <?php echo $row->juego_precio; ?> </td>
                    <td> <?php echo $row->consola_desc; ?> </td>
                    <td> <img src="<?php echo base_url('/uploads/') . $row->juego_imagen ?>" alt="" height="100" width="100" /> </td>
                    <td>
                        <?php
                        if ($this->session->userdata('login')) {
                            echo form_open('Carrito_controller/agregar_carrito');
                            echo form_hidden('id', $row->juego_id);
                            echo form_hidden('nombre', $row->juego_titulo);
                            echo form_hidden('descripcion', $row->juego_descripcion);
                            echo form_hidden('precio', $row->juego_precio);
                            echo form_submit('comprar', 'Agregar al carrito', "class='btn  btn-success'");
                            echo form_close();
                        } ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>