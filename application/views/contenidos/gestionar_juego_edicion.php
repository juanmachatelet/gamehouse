<div class="row">
    <div class="w-50 mx-auto">
        <div style="border-radius:20px;background-color:#028c4b;padding:15px;margin-top:20px">
            <h1 class="text-center">Edicion de Juegos</h1>
            <div class="container">

                <div class="w-50 mx-auto">
                    <?php echo form_open_multipart("Juego_controller/actualizar_juego/$juego_id");  ?>

                    <div class="form-group">
                        <label for="titulo">Titulo juego</label>
                        <?php echo form_input(['name' => 'titulo', 'id' => 'titulo', 'class' => 'form-control', 'placeholder' => 'Ingrese titulo del producto', 'value' => "$titulo"]) ?>
                    </div> <span class="text-danger text-uppercase"><?php echo form_error('titulo'); ?> </span>

                    <div class="form-group">
                        <label for='desarrollador'>Desarrollador</label>
                        <?php echo form_input(['name' => 'desarrollador', 'id' => 'desarrollador', 'class' => 'form-control',  'placeholder' => 'Ingrese desarrollador del producto', 'value' => "$desarrollador"]) ?> <span class="text-danger text-uppercase"><?php echo form_error('desarrollador'); ?> </span>
                    </div>

                    <div class="form-group">
                        <label for='descripcion'>Descripcion</label>
                        <?php echo form_input(['name' => 'descripcion', 'id' => 'descripcion', 'class' => 'form-control',  'placeholder' => 'Ingrese descripcion del producto', 'value' => "$descripcion"]) ?> <span class="text-danger text-uppercase"><?php echo form_error('descripcion'); ?> </span>
                    </div>

                    <div class="form-group">
                        <label for="stock">Stock disponible</label>
                        <?php echo form_input(['name' => 'stock', 'id' => 'stock', 'class' => 'form-control', 'placeholder' => 'Ingrese stock disponible', 'value' => "$stock"]) ?> <span class="text-danger text-uppercase"><?php echo form_error('stock'); ?> </span>
                    </div>

                    <div class="form-group">
                        <label for="precio">Precio</label>
                        <?php echo form_input(['name' => 'precio', 'id' => 'precio', 'class' => 'form-control', 'placeholder' => 'Ingrese precio del producto',  'value' => "$precio"])  ?> <span class="text-danger text-uppercase"><?php echo form_error('precio'); ?> </span>
                    </div>


                    <div class="form-group">
                        <img src="<?php echo base_url('/uploads/') . $imagen ?>" height="100" width="100" />
                        <label for="imagen">Imagen del producto</label>
                        <?php echo form_input(['name' => 'imagen', 'id' => 'imagen', 'type' => 'file', 'value' => 'set_value'('imagen')])  ?><span class="text-danger text-uppercase"><?php echo form_error('imagen'); ?> </span>
                    </div>
                    <div class="form-group">
                        <label for="consola">Seleccione Consola</label>
                        <?php
                        
                        foreach ($consolas as $row) {
                            $lista[$row->id_consola] = $row->consola_desc;
                        }
                        $selectedConsole = $lista[$consola];
                        echo form_dropdown('consola', $lista, $consola, 'class="form-control"'); ?>
                    </div>
                </div>
                <div class="form-group">

                    <?php echo form_submit('Editar', 'Editar', "class='btn  btn-success'"); ?>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>