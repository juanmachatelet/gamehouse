<h1 class="text-center text-white">Iniciar Sesión</h1>
<div class="container">
    <div class="row">
        <div class="container">
            <div class="w-50 mx-auto">

                <?php echo validation_errors(); ?>

                <?php echo form_open('iniciarSesion'); ?>

                <div class="form-group">
                    <label for="mail" class="text-white font-weight-bold">Ingrese su correo electrónico</label>
                    <?php echo form_input(['name' => 'mail', 'id' => 'mail', 'type' => 'email', 'class' => 'form-control', 'placeholder' => 'Ingrese Email', 'value' => set_value('mail')]); ?>
                </div>

                <div class="form-group">
                    <label for="password" class="text-white font-weight-bold">Ingrese contraseña</label>
                    <?php echo form_input(['name' => 'password', 'id' => 'password', 'type' => 'password', 'class' => 'form-control', 'placeholder' => 'Ingrese Password', 'value' => set_value('password')]); ?>
                </div>

                <div class="form-group">
                    <?php echo form_submit('Registrarme', 'Login', "class='btn  btn-success'"); ?>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>