<section class="container-fluid">
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-1"></div>
        <div class="col-sm-2" >
            <h1 style="border-bottom: 2px solid #028c4b;" class="display-6 font-weight-bold text-white text-center mb-4 mt-4">Nosotros</h1>
        </div>
        
        <div class="col-sm-5"></div>
    </div>
    <div class="row" style="margin-top:10px">
        <div class="col-sm-4"></div>
        <div class="col-sm-4" id="aboutUs">
            <h6 class="text-white" style="font-weight: 500">Sobre GAME House</p>
            <p class="aboutText">El proyecto GAME House fue desarrollado
            por una compañía internacional dedicada a la venta de videojuegos
            para PC y consolas. En el sitio puedes comprar juegos
            y ponerte al día con los últimos lanzamientos exclusivos.</p>
            <h6 class="text-white" style="font-weight: 500">Nuestro Método</p>
            <p class="aboutText">Intentamos transmitir la atmósfera <span>Gaming</span> lo máximo posible y sumergir así
            al público al fabuloso mundo de los videojuegos.</p>
        </div>
        <div class="col-sm-4"></div>
    </div>
</section>