<div class="row">
    <div class="w-50 mx-auto">
        <h1 class="text-center text-white">Registro de Juegos</h1>

        <?php echo validation_errors(); ?>
        <?php echo form_open_multipart('insertar_juego')  ?>

        <div class="form-group">
            <label for="titulo" class="text-white font-weight-bold">Ingresar Titulo</label>
            <?php echo form_input(['name' => 'titulo', 'id' => 'titulo', 'class' => 'form-control', 'placeholder' => 'Titulo', 'value' => set_value('titulo')]); ?>
        </div><span class="text-danger"><?php echo form_error('titulo'); ?> </span>

        <div class="form-group">
            <label for="desarrollador" class="text-white font-weight-bold">Ingresar Desarrollador</label>
            <?php echo form_input(['name' => 'desarrollador', 'id' => 'desarrollador', 'class' => 'form-control', 'placeholder' => 'Desarrollador', 'value' => set_value('desarrollador')]); ?>
        </div><span class="text-danger"><?php echo form_error('desarrollador'); ?> </span>

        <div class="form-group">
            <label for="descripcion" class="text-white font-weight-bold">Ingresar Descripcion</label>
            <?php echo form_input(['name' => 'descripcion', 'id' => 'descripcion', 'class' => 'form-control', 'placeholder' => 'Descripcion', 'value' => set_value('descripcion')]); ?>
        </div><span class="text-danger"><?php echo form_error('descripcion'); ?> </span>

        <div class="form-group">
            <label for="stock" class="text-white font-weight-bold">Ingresar Stock</label>
            <?php echo form_input(['name' => 'stock', 'id' => 'stock', 'class' => 'form-control', 'placeholder' => 'Stock', 'value' => set_value('stock')]); ?>
        </div><span class="text-danger"><?php echo form_error('stock'); ?> </span>

        <div class="form-group">
            <label for="precio" class="text-white font-weight-bold">Ingresar Precio</label>
            <?php echo form_input(['name' => 'precio', 'id' => 'precio', 'type' => 'int', 'class' => 'form-control', 'placeholder' => 'Precio', 'value' => set_value('precio')]); ?>
        </div><span class="text-danger"><?php echo form_error('precio'); ?> </span>

        <div class="form-group">
            <label for="imagen" class="text-white font-weight-bold">imagen</label>
            <?php echo form_input(['name' => ' imagen', 'id' => 'imagen', 'type' => 'file', 'value' => 'set_value'('imagen')])  ?> > <span class="text-danger text-uppercase"><?php echo form_error('imagen'); ?> </span>
        </div>
        <span class="text-danger"><?php echo form_error('imagen'); ?> </span>

        <div class="form-group">
            <label for="consola" class="text-white font-weight-bold">Consola</label>
            <?php
            $lista['0'] = 'Seleccione consola';
            foreach ($consolas as $row) {
                $lista[$row->id_consola] = $row->consola_desc;
            }
            echo form_dropdown('consola', $lista, '0', 'class="form-control"'); ?>
        </div> <span class="text-danger text-uppercase"><?php echo form_error('consola'); ?> </span>

        <span class="text-danger"><?php echo form_error('consola'); ?></span>


        <div class="form-group">

            <?php echo form_submit('Agregar', 'Agregar', "class='btn  btn-success'"); ?>
        </div>


        <?php echo form_close(); ?>


    </div>
</div>