<div class="container-fluid">
    <div class="row" style="padding-top: 30px">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <h1 class="text-center text-white">Realizar Consulta</h1>
            <?php echo validation_errors(); ?>


            <?php echo form_open('consultar'); ?>

            <div class="form-group">
                <label for="nombre" class="text-white font-weight-bold">Ingrese su Nombre</label>

                <?php echo form_input(['name' => 'nombre', 'id' => 'nombre', 'class' => 'form-control', 'placeholder' => 'Ingrese Nombre', 'autofocus' => 'autofocus', 'value' => set_value('nombre')]); ?>

            </div> <span class="text-danger"><?php echo form_error('nombre'); ?> </span>

            <div class="form-group">
                <label for="mail" class="text-white font-weight-bold">Ingrese su Correo Electrónico</label>
                <?php echo form_input(['name' => 'mail', 'id' => 'mail', 'type' => 'email', 'class' => 'form-control', 'placeholder' => 'Ingrese Email', 'value' => set_value('mail')]); ?>
            </div>


            <div class="form-group">
                <label for="asunto" class="text-white font-weight-bold">Ingrese Asunto de la Consulta</label>
                <?php echo form_input(['name' => 'asunto', 'id' => 'asunto', 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Ingrese Asunto', 'value' => set_value('asunto')]); ?>
            </div>

            <div class="form-group">
                <label for="contenido" class="text-white font-weight-bold">Ingrese su Consulta</label>
                <?php echo form_input(['name' => 'contenido', 'id' => 'contenido', 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Ingrese Contenido', 'value' => set_value('contenido')]); ?>
            </div>

            <div class="form-group">
                <?php $submitJS = 'onClick="submitSuccess()"'; ?>
                <!-- Deberia llamarse a la funcion solamente si la consulta fue realmente realizada, es decir, si no hubo errores -->
                <?php echo form_submit('Enviar Consulta', 'Enviar Consulta', "class='btn  btn-success', $submitJS"); ?>
            </div>

            <?php echo form_close(); ?>
        <div class="col-sm-4"></div>
    </div>
</div>
