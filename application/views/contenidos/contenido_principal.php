
    <section class="container">
        <div class="row" style="height: 75px;"></div>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-7">
                <div class="row">
                    <p class="text-white" id="ringText">86</p>
                    <svg
                        class="progress-ring"
                        width="120"
                        height="120">
                        <circle
                            class="progress-ring__circle"
                            stroke="#03d462"
                            stroke-width="5"
                            fill="transparent"
                            r="40"
                            cx="60"
                            cy="60"
                            />
                    </svg>
                    <img src="<?php echo base_url('assets/img/stars.png')?>" alt="Rating" class="thumbnail" id="stars">
                </div>
                <h1 class="display-6 font-weight-bold text-white text-left mb-4 mt-4" id="gameTitle">Assasins Creed Oddisey</h1>
                <p class="text-white" id="gameDesc">Vive la épica odisea de un héroe griego legendario.
                En Assassin's Creed® Odyssey, podrás decidir tu propio destino en la edad de oro de la antigua Grecia.</p>
                <h6 class="text-white" style="padding-left: 10px;">PLATAFORMAS: XBOXONE | PS4 | Windows 10</h1>
                <h6 class="text-white" style="padding-left: 10px;">GENEROS: Accion | Aventura | Mundo Abierto</h1>
                <button type="button" class="buyBtn">Comprar</button>
            </div>
            <div class="col-sm-4"></div>
        </div>
        <div class="row" style="padding-top: 50px">
            <div id="carouselExampleIndicators" class="carousel slide container" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active row">
                <div class="row">
                    <div class="col-6 col-md-4">
                        <div class="card" style="height: 12.325rem;">
                                <img src="<?php echo base_url('assets/img/infinite-warfare.png');?>" alt="CoD Infinite Warfare" style="height: 100%;width: 100%">
                        </div>
                    </div>
                    <div class="col-6 col-md-4">
                        <div class="card" style="height: 12.325rem;">
                            <img src="<?php echo base_url('assets/img/aco.png');?>" alt="Assasins creed odyssey" style="height: 100%;width: 100%">
                        </div>
                    </div>
                    <div class="col-md-4 d-md-block d-none">
                        <div class="card" style="height: 12.325rem;">  
                            <img src="<?php echo base_url('assets/img/starwars.png');?>" alt="Star Wars Battlefront 2" style="height: 100%;width: 100%">
                        </div>
                    </div>
                </div>
                </div>
                <div class="carousel-item">
                <div class="row">
                    <div class="col-6 col-md-4">
                        <div class="card" style="height: 12.325rem;">
                            <img src="<?php echo base_url('assets/img/horizon.jpg');?>" alt="Horizon Zero Dawn" style="height: 100%;width: 100%">
                        </div>
                    </div>
                    <div class="col-6 col-md-4">
                        <div class="card" style="height: 12.325rem;">
                            <img src="<?php echo base_url('assets/img/witcher.jpg');?>" alt="The Witcher 3" style="height: 100%;width: 100%">
                        </div>
                    </div>
                    <div class="col-md-4 d-md-block d-none">
                        <div class="card" style="height: 12.325rem;">
                            <img src="<?php echo base_url('assets/img/fallout.jpg');?>" alt="Fallout 4" style="height: 100%;width: 100%">
                        </div>
                    </div>
                </div>
                </div>
                <div class="carousel-item">
                <div class="row">
                    <div class="col-6 col-md-4">
                        <div class="card" style="height: 12.325rem;" >
                            <img src="<?php echo base_url('assets/img/gta5.jpg');?>" alt="Grand Theft Auto 5" style="height: 100%;width: 100%">
                        </div>
                    </div>
                    <div class="col-6 col-md-4">
                        <div class="card" style="height: 12.325rem;">
                            <img src="<?php echo base_url('assets/img/reddead.png');?>" alt="Red Dead Redemtion 2" style="height: 100%;width: 100%">
                        </div>
                    </div>
                    <div class="col-md-4 d-md-block d-none">
                        <div class="card" style="height: 12.325rem;">
                            <img src="<?php echo base_url('assets/img/gow.jpg');?>" alt="God of War" style="height: 100%;width: 100%">
                        </div>
                    </div>
                </div>
                </div>      
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            </div>
        </div>
    </section>
