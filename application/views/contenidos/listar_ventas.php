<div class="row">
  <div class="w-50 mx-auto">
    <div style="border-radius:20px;background-color:#028c4b;padding:20px;margin-top: 15px">
      <h1 class="text-center">Lista de Ventas</h1>
      <div class="container text-center ">
        <?php $dates=array('comienzo' => $this->input->post('txtStartDate'), 'final' => $this->input->post('txtEndDate'));?>
        <?php $comienzo = null ?>
        <?php $final = null ?>
        <form method="post" action="<?php echo base_url("filtrar") ;?>">
          <input type="date" name="txtStartDate">
          <input type="date" name="txtEndDate">
          <input type="submit" value="Filtrar Ventas" name="submit">
          <?php $comienzo = $this->input->post('txtStartDate');?>
          <?php $final = $this->input->post('txtEndDate');?>
        </form>

        <table class="mt-5 table table-bordered table-dark col-12">
          <thead>

            <th>ID venta</th>
            <th>ID cliente</th>
            <th>Nombre Cliente</th>
            <th>Fecha venta</th>
            <th>Ver detalles</th>
          </thead>
          <tbody>
            <?php foreach ($venta as $row) { ?>
              <tr>
                <td><?php echo $row->venta_id; ?></td>
                <td><?php echo $row->cliente_id; ?></td>
                <td><?php echo $row->nombre; ?></td>
                <td><?php echo $row->venta_fecha;  ?></td>
                <td><a href="<?php echo base_url("Venta_controller/detalle_ventas/$row->venta_id"); ?>" class="btn btn-success" role="button">Ver detalles</a></td>
              </tr>

            <?php }  ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>