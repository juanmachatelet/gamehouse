<div class="row">
    <div class="w-50 mx-auto">
        <div style="border-radius:20px;background-color: #028c4b;padding:15px;margin-top:20px">
            <h1 class="text-center">Carrito de compras</h1>
            <div class="container text-center ">
                <a href="<?php echo base_url('catalogo'); ?>" class="btn  btn-success">Continuar compra</a>
            </div>

            <h2 class="text-center"><?php echo $message ?></h2>

            <div class="container text-center ">
                <table class="table table-bordered table-dark col-12">
                    <?php if ($cart = $this->cart->contents()) :  ?>
                        <thead>
                            <th>N° producto</th>
                            <th>Titulo</th>
                            <th>Descripcion</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th>Subtotal</th>
                            <th>Accion</th>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($cart as $item) : ?>
                                <tr>

                                    <td><?php echo $i++;  ?></td>
                                    <td><?php echo $item['name'];  ?></td>
                                    <td><?php echo $item['descripcion'];  ?></td>
                                    <td><?php echo $item['qty']; ?></td>
                                    <td><?php echo $this->cart->format_number($item['price'], 2);  ?></td>
                                    <td><?php echo $this->cart->format_number($item['subtotal'], 2);  ?></td>
                                    <td style="color:red"> <?php echo anchor('Carrito_controller/eliminar/' . $item['rowid'], 'Eliminar'); ?> </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>

                                <td>
                                    Total Compra: $<?php echo number_format($this->cart->total(), 2); ?>

                                </td>

                                <td>
                                    <button type="button" class="btn  btn-success" onclick="limpiar_carrito()">Vaciar carrito</button>
                                </td>
                                <td>
                                    <a href="<?php echo base_url('Venta_controller/guardar_venta'); ?>" class="btn btn-success" role="button">Ordenar compra</a>
                                </td>
                            </tr>


                        <?php endif; ?>
                        </tbody>
                </table>
                <script>
                    function limpiar_carrito() {
                        var result = confirm('Desea vaciar el carrito?');
                        if (result) {
                            window.location = "<?php echo base_url(); ?>Carrito_controller/eliminar/all"
                        } else {
                            return false;
                        }
                    }
                </script>
            </div>
        </div>
    </div>
</div>