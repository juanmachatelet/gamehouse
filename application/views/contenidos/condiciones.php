<div class="container-fluid">
    <div class="row" style="margin-top: 15px;">
        <div class="col-sm-3"></div>
        <div class="col-sm-6" style="background-color: #028c4b">
            <h1 class="display-6 font-weight-bold text-white text-center mb-4 mt-4" style="border-bottom: 2px solid #028c4b;">Términos y condiciones</h1>
            <p class='text-white' style="padding-bottom: 20px;border-bottom: 2px solid #028c4b;">
            Términos y condiciones de uso de sitio web

1. ACEPTACIÓN 
En el presente documento (en adelante, el “Contrato”) se establecen los términos y condiciones de Robert Half Internacional Empresa de Servicios Transitorios Limitada, con domicilio en Avenida Isidora Goyenechea 2800 Piso 15. Torre Titanium 7550-647 Las Condes, que serán de aplicación al acceso y uso por parte del Usuario de esta página web (el  “Sitio Web”). Les rogamos lean atentamente el presente Contrato. 
Al acceder, consultar o utilizar el Sitio Web, los Usuarios (“Vd.”, “usted”, “Usuario”, o “usuario”) aceptan cumplir los términos y condiciones establecidos en este Contrato. En caso de que usted no acepte quedar vinculado por los presentes términos y condiciones, no podrá acceder a, ni utilizar, el Sitio Web. 
Robert Half Internacional Empresa de Servicios Transitorios Limitada y sus respectivas empresas afiliadas (en conjunto, “RH”) se reservan el derecho de actualizar el presente Contrato siempre que lo consideren oportuno. En consecuencia, recomendamos al Usuario revisar periódicamente las modificaciones efectuadas al Contrato.
El presente Sitio Web está dirigido exclusivamente a personas residentes en Argentina. Los Usuarios residentes o domiciliados en otro país que deseen acceder y utilizar el Sitio Web, lo harán bajo su propio riesgo y responsabilidad, por lo que deberán asegurarse de que dichos accesos y/o usos cumplen con la legislación aplicable en su país.

2. REQUISITOS RELATIVOS AL USUARIO
El Sitio Web y los servicios relacionados con el mismo se ofrecen a los Usuarios que tengan capacidad legal para otorgar contratos legalmente vinculantes según la legislación aplicable. 
Los menores no están autorizados para utilizar el Sitio Web. Si usted es menor de edad, por favor, no utilice esta web. 

3. LICENCIA 
En este acto, RH otorga al Usuario una licencia limitada, no exclusiva, intransferible, no susceptible de cesión y revocable; para consultar y descargar, de forma temporal, una copia del contenido ofrecido en el Sitio Web, únicamente para uso personal del Usuario o dentro de su empresa, y nunca con fines comerciales. 
Todo el material mostrado u ofrecido en el Sitio Web, entre otros ejemplos, el material gráfico, los documentos, textos, imágenes, sonido, video, audio, las ilustraciones, el software y el código HTML (en conjunto, el “Contenido”), es de  exclusiva propiedad de RH o de las empresas que facilitan dicho material. 
El Contenido está protegido por las leyes de copyright Argentinanas, estadounidenses e internacionales, así como por las demás leyes, reglamentos y normas aplicables a los derechos de propiedad intelectual. Salvo disposición expresa en contrario en el presente contrato, y/o salvo que por imperativo legal ello esté expresamente permitido por leyes derogatorias de las actualmente vigentes, el Usuario no podrá (i) utilizar, copiar, modificar, mostrar, eliminar, distribuir, descargar, almacenar, reproducir, transmitir, publicar, vender, revender, adaptar, invertir el proceso de creación o crear productos derivados a partir de, el Contenido. Tampoco podrá (ii) utilizar el Contenido en otras páginas Web o en cualquier medio de comunicación como, por ejemplo, en un entorno de red, sin la previa autorización por escrito en este sentido de RH.
Todas las marcas comerciales, las marcas de servicio y los logos (en adelante, las “Marcas”) mostrados en el Sitio Web son propiedad exclusiva de RH y de sus respectivos propietarios. 
El Usuario no podrá utilizar las Marcas en modo alguno sin la previa autorización por escrito para ello de RH y los respectivos propietarios.
            </p>
            <p class="text-white">
            ¿Ayuda? Si desea más información sobre el Sitio Web o sobre el presente Contrato, contactar con:
            </p>
            <p class="text-white">
            Dirección: Juan Chatelet
            </p>
            <p class="text-white">
            Dirección de correo electrónico: juanmachatelet@gmail.com
            </p>
            <p class="text-white">
            Todos los derechos reservados. 
            </p>
        </div>
        <div class="col-sm-3"></div>
    </div>
</div>