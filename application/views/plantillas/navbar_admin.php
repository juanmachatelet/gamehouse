<nav class="navbar navbar-expand-lg navbar-dark scrolling-navbar navbar-custom">
    <a class="navbar-brand" href="<?php echo base_url(); ?>">
        <img src="<?php echo base_url('assets/img/mainlogo.png') ?>" height="30" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar" aria-controls="myNavbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse flex-grow-1 text-right" id="myNavbar">
        <ul class="navbar-nav ml-auto flex-nowrap">
            <?php if (($this->session->userdata('login')) && ($this->session->userdata('perfil') == 1)) { ?>
                <li class="nav-item">
                    <a href="<?php echo base_url('agregar'); ?>" class="nav-link">Registrar productos</a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url('gestionar'); ?>" class="nav-link">Gestionar Productos</a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url('listarVen'); ?>" class="nav-link">Listar Ventas</a>
                </li>

                <li class="nav-item">
                    <a href="<?php echo base_url('listarCons'); ?>" class="nav-link">Listar Consultas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><?php echo $this->session->userdata('nombre') . ' (Admin)'; ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('salir') ?>">Salir</a>
                </li>
            <?php } else redirect('');?>
        </ul>
    </div>
</nav>