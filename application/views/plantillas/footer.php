<footer class="page-footer font-small special-color-dark pt-4" id="footer">

            <!-- Footer Elements -->
            <div class="container">
            <ul class="social-networks spin-icon">
                <li><a target="_blank" href="https://ar.linkedin.com/in/juan-chatelet-66b93212a" class="icon-linkedin">LinkedIn</a></li>
                <li><a target="_blank" href="https://twitter.com/chatelet_juan" class="icon-twitter">Twitter</a></li>
                <li><a target="_blank" href="https://www.facebook.com/JuanChatelet" class="icon-facebook">Facebook</a></li>
                <!-- <li><a href="https://twitch.tv" class="icon-twitch">Twitch</a></li>
                <li><a href="https://github.com" class="icon-github">GitHub</a></li> -->
                <!-- <li><a href="https://pinterest.com" class="icon-pinterest">Pinterest</a></li> -->
                <li><a target="_blank" href="https://instagram.com/juanchatelet" class="icon-instagram">Instagram</a></li>
                <!-- <li><a href="https://vimeo.com" class="icon-vimeo">Vimeo</a></li> -->
            </ul>

            </div>
            <!-- Footer Elements -->

            <!-- Copyright -->
            <div class="footer-copyright text-center py-3">
                <p class="text-white">© 2020 Copyright: Juan Chatelet</p>
            </div>
            <!-- Copyright -->

        </footer>
        <!-- Footer -->
        
        <!-- Librería jQuery requerida por los plugins de JavaScript -->
 		<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.4.1.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/progress-ring.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/main.js');?>"></script>
    </body>
</html>