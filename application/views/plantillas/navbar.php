<nav class="navbar navbar-expand-lg navbar-dark scrolling-navbar navbar-custom">
  <a class="navbar-brand" href="<?php echo base_url();?>">
    <img src="<?php echo base_url('assets/img/mainlogo.png')?>"  height="30" alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar"
    aria-controls="myNavbar" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse flex-grow-1 text-right" id="myNavbar">
    <ul class="navbar-nav ml-auto flex-nowrap">
      <li class="nav-item" id="principal">
        <a class="nav-link" href="<?php echo base_url();?>">Principal</a>
      </li>
      <li class="nav-item" id="nosotros">
        <a class="nav-link" href="<?php echo base_url('nosotros');?>">Quienes Somos</a>
      </li>
      <li class="nav-item" id="contacto">
        <a class="nav-link" href="<?php echo base_url('contacto');?>">Contacto</a>
      </li>
      <li class="nav-item" id="condiciones">
        <a class="nav-link" href="<?php echo base_url('condiciones');?>">Terminos y Condiciones</a>
      </li>
      <li class="nav-item" id="productos">
        <a class="nav-link" href="<?php echo base_url('catalogo');?>">Catálogo</a>
      </li>
      <?php if($this->session->userdata('login')){?>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('carrito')?>">Ver Carrito</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><?php echo $this->session->userdata('nombre');?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('salir')?>">Salir</a>
        </li>
      <?php } else { ?>
        <li class="nav-item" id="registrarse">
        <a class="nav-link" href="<?php echo base_url('registro');?>">Registrarse</a>
      </li>
      <li class="nav-item" id="login">
        <a class="nav-link" href="<?php echo base_url('login');?>">Login</a>
      </li>
      <?php }?>
      
    </ul>
  </div>
</nav>
