<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
 		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1,
		shrink-to-fit=no">
        <title><?php echo($titulo);?></title>
        <!-- Logotipo de la pestaña -->
        <link rel="icon" href="<?php echo base_url('assets/img/pagelogo.png')?>">
 		<!-- Bootstrap -->
        <link type="text/css" href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
        <!-- Estilo Propio -->
        <link type="text/css" href="<?php echo base_url('assets/css/miestilo.css');?>" rel="stylesheet">
        <!-- Iconos -->
        <link type="text/css" href="<?php echo base_url('assets/css/icons.css');?>" rel="stylesheet">
        <!-- Fuentes (letras) -->
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@700&display=swap" rel="stylesheet">
    </head>
    <body>