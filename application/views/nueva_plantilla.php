<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
 		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1,
		shrink-to-fit=no">
 		<title>Juan Chatelet</title>
 		<!-- Bootstrap -->
         <link type="text/css" href="<?php echo base_url('assets/css/bootstrap.css');?>" rel="stylesheet">
         <style>
            body {
                margin: 0;
                padding: 0;
                background: linear-gradient(#00a8cc, #0c7b93);
            }
            .carousel-inner img {
                width: 100%;
                height: 100%;
            }
        </style>
    </head>
    <body>
        <header class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="d-flex flex-grow-1">
                    <span class="w-100 d-lg-none d-block"><!-- hidden spacer to center brand on mobile --></span>
                    <a class="navbar-brand d-none d-lg-inline-block" href="#">
                         Juan Chatelet
                     </a>
                    <a class="navbar-brand-two mx-auto d-lg-none d-inline-block" href="#">
                        <img src="./assets/img/logomain.png" alt="logo">
                    </a>
                    <div class="w-100 text-right">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </div>
                <div class="collapse navbar-collapse flex-grow-1 text-right" id="myNavbar">
                    <ul class="navbar-nav ml-auto flex-nowrap">
                        <li class="nav-item">
                            <a href="#" class="nav-link m-2 menu-item nav-active">Sobre mi</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link m-2 menu-item">Experiencia</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link m-2 menu-item">Blog</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link m-2 menu-item">Contacto</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <h1 class="display-3 text-center mb-4 mt-4">Bienvenidos!</h1>
        <section class="container">
            
            <div id="demo" class="carousel slide" data-ride="carousel">

                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0"></li>
                    <li data-target="#demo" data-slide-to="1" class="active"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item">
                        <img src="./assets/img/1.jpg" alt="Los Angeles">
                    </div>
                    <div class="carousel-item active">
                        <img src="./assets/img/2.jpg" alt="Chicago">
                    </div>
                    <div class="carousel-item">
                        <img src="./assets/img/3.jpg" alt="New York">
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>

            </div>
            
        </section>
        <section class="container">
            <h1 class="text-center mt-5 mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>
            <p class="text-center mt-1">Donec sit amet purus quis nibh iaculis ultricies venenatis id enim.</p>
            <p class="text-center mt-1">Etiam molestie quis ante et mollis.</p>
            <p class="text-center mt-1">Mauris tellus nunc, volutpat at tortor et, facilisis porta risus.</p>
        </section>
        <!-- Footer -->
        <footer class="page-footer font-small special-color-dark pt-4">

            <!-- Footer Elements -->
            <div class="container">

            <!-- Social buttons -->
            <ul class="list-unstyled list-inline text-center">
                <li class="list-inline-item">
                    <a class="btn-floating btn-fb mx-1" href="https://www.facebook.com/JuanChatelet">
                        <img src="./assets/img/fb.png" alt="Facebook">
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-tw mx-1" href="https://twitter.com/chatelet_juan">
                        <img src="./assets/img/tw.png" alt="Twitter">
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-li mx-1" href="https://ar.linkedin.com/in/juan-chatelet-66b93212a">
                        <img src="./assets/img/li.png" alt="LinkedIn">
                    </a>
                </li>
            </ul>
            <!-- Social buttons -->

            </div>
            <!-- Footer Elements -->

            <!-- Copyright -->
            <div class="footer-copyright text-center py-3">
                <p>© 2020 Copyright: Juan Chatelet</p>
            </div>
            <!-- Copyright -->

        </footer>
        <!-- Footer -->
        
        <!-- Librería jQuery requerida por los plugins de JavaScript -->
 		<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.4.1.min.js');?>"></script>
 		<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
    </body>
</html>
