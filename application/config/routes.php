<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Proyecto_controller/index';
$route['nosotros'] = 'Proyecto_controller/nosotros';
$route['catalogo'] = 'Proyecto_controller/catalogo';
$route['condiciones'] = 'Proyecto_controller/condiciones';

$route['contacto'] = 'Consultas_controller/contacto';
$route['consultar'] = 'Consultas_controller/realizar_consulta';

$route['registro'] = 'usuarios_controller/registro';
$route['registrarse'] = 'usuarios_controller/registrar_usuario';
$route['login'] = 'usuarios_controller/login';
$route['iniciarSesion'] = 'usuarios_controller/iniciar_sesion';
$route['salir'] = "usuarios_controller/cerrar_sesion";

$route['agregar'] = 'Juego_controller/form_agregar_juego';
$route['administracion'] = 'Admin_controller/indexAdmin';
$route['insertar_juego'] = 'Juego_controller/registrar_juego';
$route['gestionar'] = 'Juego_controller/gestionar_juego';

$route['carrito'] = 'Carrito_controller/ver_carrito';
$route['comprar'] = 'Venta_controller/guardar_venta';
$route['listarVen'] = 'Venta_controller/listar_ventas';
$route['listarCons'] = 'Consultas_controller/listar_consultas';
$route['detalles'] = "Venta_controller/detalles";
$route['filtrar'] = "Venta_controller/filtrar_ventas";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
