<?php

class juego_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function guardar_juego($data)
    {
        $this->db->insert('juegos', $data);
    }
    public function select_consola()
    {
        $query = $this->db->get('juegos_consola');
        return $query->result();
    }

    public function select_juego()
    {
        $this->db->select('*');
        $this->db->from('juegos');
        $this->db->join('juegos_consola', 'juegos_consola.id_consola=juegos.consola_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function select_juegos_id($id)
    {
        $this->db->select('*');
        $this->db->from('juegos');
        $this->db->where('juego_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_juegos()
    {
        $this->db->select('*');
        $this->db->from('juegos');

        $this->db->where('juego_stock >', 0);
        $this->db->where('juego_estado', 1);

        $this->db->join('juegos_consola', 'juegos_consola.id_consola= juegos.consola_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_juego_id($id)
    {
        $this->db->select('*');
        $this->db->from('juegos');
        $this->db->where('juego_id', $id);
        $query = $this->db->get();
        $resultado = $query->row();
        return $resultado;
    }


    public function actualizar_juego($data, $id)
    {
        $this->db->where('juego_id', $id);
        $this->db->update('juegos', $data);
    }
}
