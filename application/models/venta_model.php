<?php

class venta_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function guardar_ventas($data)
    {
        $this->db->insert('venta', $data);
    }
    public function guardar_detalle_ventas($data)
    {
        $this->db->insert('detalle_venta', $data);
    }

    public function select_ventas()
    {
        $this->db->select('*');
        $this->db->from('venta');
        $this->db->join('personas', 'personas.id_persona = venta.cliente_id');
        $query = $this->db->get();
        return $query->result();
    }
    public function select_detalle_ventas($id)
    {
        $this->db->select('*');
        $this->db->from('detalle_venta');
        $this->db->where('id_venta', $id);
        $this->db->join('venta', 'venta.venta_id = detalle_venta.id_venta');
        $this->db->join('juegos', 'juegos.juego_id = detalle_venta.id_juego');
        $query = $this->db->get();
        return $query->result();
    }

    public function select_filtered_dates($start, $end){
        $this->db->select('*');
        $this->db->from('venta');
        $this->db->where('venta_fecha >=', $start);
        $this->db->where('venta_fecha <=', $end);
        $this->db->join('personas', 'personas.id_persona = venta.cliente_id');
        $query = $this->db->get();
        return $query->result();
    }
}
